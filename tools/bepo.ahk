﻿#NoEnv  ; Recommended for performance and compatibility with future AutoHotkey releases.
; #Warn  ; Enable warnings to assist with detecting common errors.
SendMode Input  ; Recommended for new scripts due to its superior speed and reliability.
SetWorkingDir %A_ScriptDir%  ; Ensures a consistent starting directory.

SetTitleMatchMode, 1
SetControlDelay -1
 
winTitleInstall = KbdEdit Layout Installer

 
WinWait, %winTitleInstall%, , 300
WinActivate
; BlockInput, Off

sleep, 3000
 
; Install Screen
WinWait, %winTitleInstall%, Installer, 10, 
ControlClick, &Install, %winTitleInstall%,,,, NA
Sleep, 1000
WinWait, %winTitleInstall%, successfully, 10, 
ControlClick, OK, %winTitleInstall%,,,, NA