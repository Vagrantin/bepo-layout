﻿
$ErrorActionPreference = 'Stop'; # stop on all errors
$toolsDir   = "$(Split-Path -parent $MyInvocation.MyCommand.Definition)"
$fileLocation = Join-Path $toolsDir 'bepo-1.1rc2-full.exe'



$packageArgs = @{
  packageName   = $env:ChocolateyPackageName
  unzipLocation = $toolsDir
  fileType      = 'exe' #only one of these: exe, msi, msu
  file         = $fileLocation

  softwareName  = 'bepo*' #part or all of the Display Name as you see it in Programs and Features. It should be enough to be unique

  checksum      = '73FBC78F39A710A2D6B9400EDE589410F9EF155AC9F8517BF6D7C21DC7CA63DC'
  checksumType  = 'sha256' #default is md5, can also be sha1, sha256 or sha512
  checksum64    = '73FBC78F39A710A2D6B9400EDE589410F9EF155AC9F8517BF6D7C21DC7CA63DC'
  checksumType64= 'sha256' #default is checksumType

  # MSI
  silentArgs    = ""
  validExitCodes= @(0)

}

#Thanks to dtgm and the GitHub package for ideas.
$ahkExe = 'AutoHotKey'
$ahkFile = Join-Path $toolsDir "bepo.ahk"
$ahkProc = Start-Process -FilePath $ahkExe `
                         -ArgumentList $ahkFile `
                         -PassThru
 
$ahkId = $ahkProc.Id
Write-Debug "$ahkExe start time:`t$($ahkProc.StartTime.ToShortTimeString())"
Write-Debug "Process ID:`t$ahkId"

Install-ChocolateyInstallPackage @packageArgs # https://chocolatey.org/docs/helpers-install-chocolatey-install-package


