﻿
$ErrorActionPreference = 'Stop'; # stop on all errors
$toolsDir   = "$(Split-Path -parent $MyInvocation.MyCommand.Definition)"
$fileLocation = Join-Path $toolsDir 'bepo-1.1rc2-full.exe'


$packageArgs = @{
  packageName   = $env:ChocolateyPackageName
  softwareName  = 'bepo*'  #part or all of the Display Name as you see it in Programs and Features. It should be enough to be unique
  fileType      = 'exe' #only one of these: MSI or EXE (ignore MSU for now)
  silentArgs    = ""
  file         = $fileLocation
  validExitCodes= @(0) # https://msdn.microsoft.com/en-us/library/aa376931(v=vs.85).aspx

}

#Thanks to dtgm and the GitHub package for ideas.
$ahkExe = 'AutoHotKey'
$ahkFile = Join-Path $toolsDir "remove_bepo.ahk"
$ahkProc = Start-Process -FilePath $ahkExe `
                         -ArgumentList $ahkFile `
                         -PassThru
 
$ahkId = $ahkProc.Id
Write-Debug "$ahkExe start time:`t$($ahkProc.StartTime.ToShortTimeString())"
Write-Debug "Process ID:`t$ahkId"

$uninstalled = $false

Uninstall-ChocolateyPackage @packageArgs


