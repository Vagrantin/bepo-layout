﻿#NoEnv  ; Recommended for performance and compatibility with future AutoHotkey releases.
; #Warn  ; Enable warnings to assist with detecting common errors.
SendMode Input  ; Recommended for new scripts due to its superior speed and reliability.
SetWorkingDir %A_ScriptDir%  ; Ensures a consistent starting directory.

SetTitleMatchMode, 1
SetControlDelay -1
 
winTitleInstall = KbdEdit Layout Installer

 
WinWait, %winTitleInstall%, , 300
WinActivate
; BlockInput, Off

sleep, 3000
 
; Install Screen
WinWait, %winTitleInstall%, Installer, 10, 
ControlClick, Add to language bar list, %winTitleInstall%,,,, NA
ControlClick, &Uninstall >, %winTitleInstall%,,,, NA
Sleep, 3000
; Close Installer
ControlClick, &Close, %winTitleInstall%,,,, NA